const translations = {
  fr: {
    accueil: "Accueil",
    apropos: "À propos de moi",
    experiences: "Expériences",
    formation: "Formation",
    competences: "Compétences",
    contact: "Contact",
    accueilTitle: "Je suis Hugo PRADIER, <br> Alternant Développeur",
    accueilSubtitle: "Bienvenue sur mon portfolio!",
    telechargerCV: "Télécharger mon CV",
    contactezMoi: "Contactez-moi",
    bonjourTitle: "Bonjour, je m'appelle Hugo PRADIER",
    bonjourContent1:
      "Étudiant en troisième année de BUT Production Informatique à l'IUT de Clermont-Ferrand, je suis toujours en quête de nouveaux défis et compétences. Passionné par l'informatique, j'explore divers domaines avec une fascination particulière pour le monde du web.",
    bonjourContent2:
      'Titulaire d\'un Baccalauréat mention Bien, mes spécialités étaient "Mathématiques" et "N.S.I" donc j’ai continué sur un Bachelor Universitaire et Technologique en Production Informatique.',
    bonjourContent3:
      "Au fil de mes expériences, j'ai acquis une expertise dans un large éventail de langages de programmation, de bases de données et de frameworks.",
    bonjourContent4:
      "Dans ce portfolio, je vous invite à découvrir en détail mes compétences, ma formation et mes diverses expériences.",
    bonjourContent5:
      "N'hésitez pas à me contacter pour toute question ou opportunité de collaboration.",
    bonjourContent6: "Merci de votre visite !",
    experiencesTitle: "Expériences",
    sylinkTitle: "Alternance chez Sylink Technologie",
    sylinkDescription: `
      <strong>Nom de l'entreprise :</strong> Sylink Technologie<br />
      <strong>Situation géographique :</strong> Clermont-Ferrand, 63<br />
      <strong>Secteur d’activité :</strong> Informatique : conception et édition de solutions de cybersécurité innovantes.<br />
      <strong>Fonction exercée :</strong> Alternant développeur<br />
      <strong>Nom et fonction du maître de stage :</strong> Rémy SIMON, Chef de projet, développeur C#/.Net<br />
      <strong>Dates de l'expérience :</strong> du 1er septembre 2023 au 31 août 2024 (1 an)
    `,
    cegiTitle: "Stage chez Cegi Alfa",
    cegiDescription: `
      <strong>Nom de l'entreprise :</strong> Cegi Alfa<br />
      <strong>Situation géographique :</strong> Clermont-Ferrand, 63<br />
      <strong>Secteur d’activité :</strong> Informatique : développement de logiciels de gestion dans les domaines du médico-social, social et sanitaire.<br />
      <strong>Fonction exercée :</strong> Stagiaire analyste programmeur<br />
      <strong>Nom et fonction du maître de stage :</strong> Djamel MASSAÏD, Chef d’équipe développeur du projet AgileS<br />
      <strong>Dates de l'expérience :</strong> du 11 avril au 17 juin 2023 (10 semaines)
    `,
    development: "Développement",
    documentation: "Documentation",
    collaboration: "Collaboration",
    projectManagement: "Conduire un projet",
    titleSection1Cegi: "Développement chez Cegi Alfa",
    text1Section1Cegi: `Lors de ma deuxième année de BUT Production Informatique, j’ai réalisé un stage de dix semaines au sein de l’entreprise CEGI Alfa qui portait sur le développement d'une API (Application Programming Interface) en Java Spring pour permettre l'interfaçage entre deux applications du groupe.`,
    text2Section1Cegi: `J'ai consolidé mes compétences techniques dans plusieurs domaines clés. En premier lieu, j'ai approfondi ma maîtrise de Java, en particulier en l'associant à Spring Boot, une combinaison souvent utilisée pour le développement d'applications robustes et efficaces.`,
    titleSection2Cegi: "Concevoir et documenter chez Cegi Alfa",
    text1Section2Cegi: `J'ai également appliqué le patron d'architecture MVC, qui offre une structure organisée et modulaire pour une conception évolutive. Les diagrammes UML et graphiques établis en amont m'ont été précieux pour concevoir une application structurée et efficace, favorisant une meilleure optimisation du développement.`,
    text2Section2Cegi: `Parallèlement, j'ai bénéficié de l'utilisation de méthodes telles que Merise pour établir des liens cohérents entre les bases de données, assurant ainsi une gestion efficace des données et une intégration harmonieuse avec l'application. Ma compréhension avancée du langage SQL m'a été utile pour créer des requêtes complexes et optimisées dans Microsoft SQL Server Management.`,
    text3Section2Cegi: `Enfin, j'ai produit une documentation technique en utilisant Swagger, une plateforme pour la création de spécifications et de documentation claires et précises pour les API. Cet outil m'a permis de fournir des informations détaillées sur le fonctionnement de mon API, facilitant ainsi son utilisation et sa maintenance pour les développeurs ultérieurs.`,
    titleSection3Cegi: "Conduire un projet chez Cegi Alfa",
    text1Section3Cegi: `Bien qu’ayant rencontré quelques défis initiaux, notamment liés à l'exploration approfondie des technologies et à des modifications imprévues, j'ai su surmonter ces obstacles et aboutir au développement d'une API fonctionnelle. Cette dernière est conçue pour enregistrer et manipuler les données conformément aux spécifications du projet. Plus précisément, elle offre à l'utilisateur la possibilité de créer et de mettre à jour un dossier d'usager, ainsi que de générer et de modifier une demande de prise en charge.`,
    text2Section3Cegi: `Pour l'avenir, l'ajout d'un système d'authentification à l'API apparaît comme une piste prometteuse. Cette mesure permettrait de renforcer la sécurité et l'interopérabilité du système, ouvrant ainsi la voie à de nouvelles possibilités et à une évolution continue. Elle correspondait à la dernière tâche de ma planification où je me suis retrouvé en autonomie.`,
    titleSection1Sylink: "Développement chez Sylink Technologie",
    text1Section1Sylink: `En troisième année de BUT Informatique, au cours de mon alternance d'un an chez Sylink Technologie, j’ai travaillé sur deux projets distincts visant à renforcer les capacités de sécurité des entreprises. Le premier consistait à développer une API en ASP .NET Core pour faciliter la récupération d'informations sur les appareils au sein du réseau d’une organisation à partir de bases de données sous PostgreSQL ou QuestDB. Cette API permettait le traitement des données au format JSON pour la visualisation, la détection et l'analyse de menaces potentielles.`,
    text2Section1Sylink: `Simultanément, j’ai conduit un projet console en C# qui utilisait Dehashed, un moteur de recherche de bases de données piratées. Ce projet impliquait la création d'un système permettant à l'utilisateur de sélectionner une organisation pour générer des rapports sur les fuites de données piratées.`,
    text3Section1Sylink: `J'ai ensuite déployé ces deux projets pour tester avec de vraies données et partager à mes collaborateurs mes avancées grâce à l’utilisation du serveur WEB Microsoft IIS. Mais, avant cela, j’ai dû configurer une machine sur Proxmox. Je m’occupais de versionner mes projets pour communiquer plus facilement dessus.`,
    titleSection2Sylink: "Concevoir et documenter chez Sylink Technologie",
    text1Section2Sylink: `Les diagrammes UML et les graphiques établis en amont ont été précieux pour concevoir des applications structurées et efficaces, favorisant une meilleure optimisation du développement.`,
    text2Section2Sylink: `Parallèlement, j'ai utilisé des méthodes telles que Merise pour établir des liens cohérents entre les bases de données, assurant ainsi une gestion efficace des données et une intégration harmonieuse avec l'application. Ma maîtrise avancée du langage SQL m'a permis de créer des requêtes complexes et optimisées.`,
    text3Section2Sylink: `Enfin, j'ai proposé d'intégrer une documentation Swagger dans mon projet d'API, comme je l'avais appris lors de mon stage précédent chez Cegi Alfa. Cette documentation était essentielle pour faciliter la communication avec les parties prenantes et assurer la pérennité du projet, même en cas de reprise par un autre développeur.`,
    titleSection3Sylink: "Collaborer chez Sylink Technologie",
    text1Section3Sylink: `J'ai travaillé de manière autonome sur ces projets, tout en recevant des retours réguliers de mon maître d'alternance. J’ai également été amené à communiquer avec une entreprise prestataire s’occupant de la visualisation 3D. J’ai ainsi pu mettre en pratique et améliorer mes connaissances en communication. J'ai aussi eu l’occasion de communiquer avec d'autres collègues spécialisés dans des technologies spécifiques.`,

    butName: "BUT Production Informatique",
    butInfo:
      "IUT de Clermont-Ferrand, 63 <br /> <strong>Spécialité :</strong> Développement WEB",
    bacName: "Baccalauréat Général",
    bacInfo:
      "Lycée George Sand, 36 <br /> <strong>Spécialités :</strong> N.S.I / Mathématiques / Physique Chimie <br /> <strong>Mention :</strong> Bien",
    brevetName: "Diplôme National du Brevet des collèges",
    brevetInfo:
      "Collège Vincent Rotinat, 36 <br /> <strong>Mention :</strong> Très bien",
    language: "Langues",
    french: "Français",
    english: "Anglais",
    spanish: "Espagnol",
    nativeLanguage: "Langue maternelle",
    intermediateAdvanced: "Intermédiaire avancé (B2)",
    intermediate: "Intermédiaire (B1)",
    tools: "Outils",
    firstname: "Prénom",
    name: "Nom",
    email: "Email",
    phone: "Téléphone",
    form: "Formulaire",
    send: "Envoyer",

    legal: "Mentions Légales",
    edit: "Édition du site",
    descEdit: `Le présent site, accessible à l’URL <a href="https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/">https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/</a>, est édité par :`,
    hugo: `Hugo PRADIER, résidant 11 Rue de l'Hôtel de Ville 63110 BEAUMONT, de nationalité Française, né le 07/03/2003.`,
    hosting: "Hébergement",
    hostingDesc: `Le Site est hébergé par la société GitHub, possédant son siège social à San Francisco.`,
    admin: "Administrateur du site",
    adminDesc: `L'administrateur du site est Hugo PRADIER.`,
    contactMe: "Me contacter",
    phoneContact: "Par téléphone : +33768361359",
    emailContact: `Par email :`,
    postalContact: `Par courrier : 11 Rue de l'Hôtel de Ville 63110 BEAUMONT`,
    infoSection: "Informations",
    selectedLanguage: "Langue",
  },
  en: {
    accueil: "Home",
    apropos: "About Me",
    experiences: "Experiences",
    formation: "Education",
    competences: "Skills",
    contact: "Contact",
    accueilTitle: "I am Hugo PRADIER, <br> Alternating Developer",
    accueilSubtitle: "Welcome to my portfolio!",
    telechargerCV: "Download my CV",
    contactezMoi: "Contact me",
    bonjourTitle: "Hello, my name is Hugo PRADIER",
    bonjourContent1:
      "Student in the third year of the University Technology Diploma in Computer Production at the IUT of Clermont-Ferrand, I am always looking for new challenges and skills. Passionate about computer science, I explore various fields with a particular fascination for the web.",
    bonjourContent2:
      'Holder of a Baccalaureate with honors, my specialties were "Mathematics" and "N.S.I" so I continued with a University and Technological Bachelor in Computer Production.',
    bonjourContent3:
      "Through my experiences, I have acquired expertise in a wide range of programming languages, databases, and frameworks.",
    bonjourContent4:
      "In this portfolio, I invite you to discover in detail my skills, my training, and my various experiences.",
    bonjourContent5:
      "Do not hesitate to contact me for any questions or collaboration opportunities.",
    bonjourContent6: "Thank you for your visit!",
    experiencesTitle: "Experiences",
    sylinkTitle: "Internship at Sylink Technologie",
    sylinkDescription: `
      <strong>Company Name:</strong> Sylink Technologie<br />
      <strong>Location:</strong> Clermont-Ferrand, 63<br />
      <strong>Industry:</strong> IT: design and development of innovative cybersecurity solutions.<br />
      <strong>Position:</strong> Intern Developer<br />
      <strong>Supervisor Name and Position:</strong> Rémy SIMON, Project Manager, C#/.Net Developer<br />
      <strong>Experience Dates:</strong> from September 1, 2023 to August 31, 2024 (1 year)
    `,
    cegiTitle: "Internship at Cegi Alfa",
    cegiDescription: `
      <strong>Company Name:</strong> Cegi Alfa<br />
      <strong>Location:</strong> Clermont-Ferrand, 63<br />
      <strong>Industry:</strong> IT: development of management software in the fields of social, medical-social, and health.<br />
      <strong>Position:</strong> Intern Analyst Programmer<br />
      <strong>Supervisor Name and Position:</strong> Djamel MASSAÏD, Team Leader Developer of AgileS Project<br />
      <strong>Experience Dates:</strong> from April 11 to June 17, 2023 (10 weeks)
    `,
    development: "Development",
    documentation: "Documentation",
    collaboration: "Collaboration",
    projectManagement: "Project Management",
    titleSection1Cegi: "Development at Cegi Alfa",
    text1Section1Cegi: `During my second year of University Technology Diploma in Computer Production, I completed a ten-week internship at CEGI Alfa focusing on the development of an API (Application Programming Interface) in Java Spring to allow interfacing between two group applications.`,
    text2Section1Cegi: `I consolidated my technical skills in several key areas. First, I deepened my mastery of Java, especially by combining it with Spring Boot, a combination often used for the development of robust and efficient applications.`,
    titleSection2Cegi: "Design and Document at Cegi Alfa",
    text1Section2Cegi: `I also applied the MVC architecture pattern, which provides an organized and modular structure for scalable design. The UML diagrams and graphics established in advance were invaluable to me in designing a structured and efficient application, promoting better development optimization.`,
    text2Section2Cegi: `At the same time, I benefited from the use of methods such as Merise to establish coherent links between databases, thus ensuring efficient data management and harmonious integration with the application. My advanced understanding of the SQL language was useful for creating complex and optimized queries in Microsoft SQL Server Management.`,
    text3Section2Cegi: `Finally, I produced technical documentation using Swagger, a platform for creating clear and precise specifications and documentation for APIs. This tool allowed me to provide detailed information on the operation of my API, facilitating its use and maintenance for subsequent developers.`,
    titleSection3Cegi: "Project Management at Cegi Alfa",
    text1Section3Cegi: `Although I encountered some initial challenges, notably related to in-depth exploration of technologies and unforeseen modifications, I was able to overcome these obstacles and develop a functional API. The latter is designed to record and manipulate data in accordance with project specifications. More specifically, it offers the user the ability to create and update a user file, as well as generate and modify a support request.`,
    text2Section3Cegi: `For the future, adding an authentication system to the API appears as a promising track. This measure would strengthen the security and interoperability of the system, opening the way to new possibilities and continuous evolution. It corresponded to the last task of my planning where I found myself in autonomy.`,

    titleSection1Sylink: "Development at Sylink Technologie",
    text1Section1Sylink: `In the third year of my Computer Production University Technology Diploma, during my one-year alternation at Sylink Technologie, I worked on two distinct projects aimed at strengthening the security capabilities of companies. The first was to develop an API in ASP .NET Core to facilitate the retrieval of information about devices within an organization's network from databases under PostgreSQL or QuestDB. This API allowed data processing in JSON format for visualization, detection, and analysis of potential threats.`,
    text2Section1Sylink: `Simultaneously, I led a console project in C# that used Dehashed, a hacked database search engine. This project involved creating a system that allowed the user to select an organization to generate reports on hacked data leaks.`,
    text3Section1Sylink: `I then deployed these two projects to test with real data and share my progress with my colleagues using the Microsoft IIS WEB server. But before that, I had to set up a machine on Proxmox. I was versioning my projects to communicate more easily about them.`,
    titleSection2Sylink: "Design and Document at Sylink Technologie",
    text1Section2Sylink: `The UML diagrams and graphics established in advance were invaluable to design structured and efficient applications, promoting better development optimization.`,
    text2Section2Sylink: `At the same time, I used methods such as Merise to establish coherent links between databases, ensuring efficient data management and harmonious integration with the application. My advanced mastery of the SQL language allowed me to create complex and optimized queries.`,
    text3Section2Sylink: `Finally, I proposed to integrate Swagger documentation into my API project, as I had learned during my previous internship at Cegi Alfa. This documentation was essential to facilitate communication with stakeholders and ensure the sustainability of the project, even in the event of takeover by another developer.`,
    titleSection3Sylink: "Collaboration at Sylink Technologie",
    text1Section3Sylink: `I worked independently on these projects, while receiving regular feedback from my alternating supervisor. I also had to communicate with an external company responsible for 3D visualization. This allowed me to put into practice and improve my communication skills. I also had the opportunity to communicate with other colleagues specializing in specific technologies.`,

    butName: "University Technology Diploma in Computer Production",
    butInfo:
      "IUT of Clermont-Ferrand, 63 <br /> <strong>Specialty:</strong> WEB Development",
    bacName: "High School Diploma",
    bacInfo:
      "Lycée George Sand, 36 <br /> <strong>Specialties:</strong> N.S.I / Mathematics / Physics Chemistry <br /> <strong>Honors:</strong> Good",
    brevetName: "National Diploma of the Brevet des collèges",
    brevetInfo:
      "Collège Vincent Rotinat, 36 <br /> <strong>Honors:</strong> Very good",

    language: "Languages",
    french: "French",
    english: "English",
    spanish: "Spanish",
    nativeLanguage: "Native language",
    intermediateAdvanced: "Intermediate advanced (B2)",
    intermediate: "Intermediate (B1)",
    tools: "Tools",

    firstname: "Firstname",
    name: "Name",
    email: "Email",
    phone: "Phone",
    form: "Form",
    send: "Send",

    legal: "Legal Notice",
    edit: "Site Edition",
    descEdit: `This site, accessible at the URL <a href="https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/">https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/</a>, is edited by:`,
    hugo: `Hugo PRADIER, residing at 11 Rue de l'Hôtel de Ville 63110 BEAUMONT, of French`,
    hosting: "Hosting",
    hostingDesc: `The Site is hosted by the company GitHub, with its headquarters in San Francisco.`,
    admin: "Site Administrator",
    adminDesc: `The site administrator is Hugo PRADIER.`,
    contactMe: "Contact Me",
    phoneContact: "By phone: +33768361359",
    emailContact: `By email: <a href="mailto:pradier.hugo36@gmail.com">pradier.hugo36@gmail.com</a>`,
    postalContact: `By mail: 11 Rue de l'Hôtel de Ville 63110 BEAUMONT`,
    infoSection: "Informations",
    selectedLanguage: "Language",
  },
  es: {
    accueil: "Inicio",
    apropos: "Sobre mí",
    experiences: "Experiencias",
    formation: "Educación",
    competences: "Habilidades",
    contact: "Contacto",
    accueilTitle: "Soy Hugo PRADIER, <br> Desarrollador en alternancia",
    accueilSubtitle: "¡Bienvenido a mi portfolio!",
    telechargerCV: "Descargar mi CV",
    contactezMoi: "Contáctame",
    bonjourTitle: "Hola, mi nombre es Hugo PRADIER",
    bonjourContent1:
      "Estudiante en el tercer año del Diploma Universitario de Tecnología en Producción Informática en el IUT de Clermont-Ferrand, siempre estoy en busca de nuevos desafíos y habilidades. Apasionado por la informática, exploro varios campos con una fascinación particular por el mundo del web.",
    bonjourContent2:
      'Titular de un Bachillerato con honores, mis especialidades eran "Matemáticas" y "N.S.I" por lo que continué con un Bachelor Universitario y Tecnológico en Producción Informática.',
    bonjourContent3:
      "A través de mis experiencias, he adquirido experiencia en una amplia gama de lenguajes de programación, bases de datos y frameworks.",
    bonjourContent4:
      "En este portfolio, te invito a descubrir en detalle mis habilidades, mi formación y mis diversas experiencias.",
    bonjourContent5:
      "No dudes en contactarme para cualquier pregunta u oportunidad de colaboración.",
    bonjourContent6: "¡Gracias por tu visita!",
    experiencesTitle: "Experiencias",
    sylinkTitle: "Prácticas en Sylink Technologie",
    sylinkDescription: `
      <strong>Nombre de la empresa:</strong> Sylink Technologie<br />
      <strong>Ubicación:</strong> Clermont-Ferrand, 63<br />
      <strong>Industria:</strong> TI: diseño y desarrollo de soluciones innovadoras de ciberseguridad.<br />
      <strong>Cargo:</strong> Desarrollador en alternancia<br />
      <strong>Nombre del supervisor y cargo:</strong> Rémy SIMON, Jefe de proyecto, Desarrollador C#/.Net<br />
      <strong>Fechas de la experiencia:</strong> del 1 de septiembre de 2023 al 31 de agosto de 2024 (1 año)
    `,
    cegiTitle: "Prácticas en Cegi Alfa",
    cegiDescription: `
      <strong>Nombre de la empresa:</strong> Cegi Alfa<br />
      <strong>Ubicación:</strong> Clermont-Ferrand, 63<br />
      <strong>Industria:</strong> TI: desarrollo de software de gestión en los campos social, médico-social y de la salud.<br />
      <strong>Cargo:</strong> Becario analista programador<br />
      <strong>Nombre del supervisor y cargo:</strong> Djamel MASSAÏD, Líder de equipo desarrollador del proyecto AgileS<br />
      <strong>Fechas de la experiencia:</strong> del 11 de abril al 17 de junio de 2023 (10 semanas)
    `,
    development: "Desarrollo",
    documentation: "Documentación",
    collaboration: "Colaboración",
    projectManagement: "Gestión de proyectos",
    titleSection1Cegi: "Desarrollo en Cegi Alfa",
    text1Section1Cegi: `Durante mi segundo año del Diploma Universitario de Tecnología en Producción Informática, realicé una pasantía de diez semanas en CEGI Alfa centrada en el desarrollo de una API (Interfaz de Programación de Aplicaciones) en Java Spring para permitir la interfaz entre dos aplicaciones del grupo.`,
    text2Section1Cegi: `Consolidé mis habilidades técnicas en varias áreas clave. En primer lugar, profundicé mi dominio de Java, especialmente al combinarlo con Spring Boot, una combinación a menudo utilizada para el desarrollo de aplicaciones robustas y eficientes.`,
    titleSection2Cegi: "Diseño y Documentación en Cegi Alfa",
    text1Section2Cegi: `También apliqué el patrón de arquitectura MVC, que proporciona una estructura organizada y modular para un diseño escalable. Los diagramas UML y gráficos establecidos de antemano fueron invaluables para mí en el diseño de una aplicación estructurada y eficiente, promoviendo una mejor optimización del desarrollo.`,
    text2Section2Cegi: `Al mismo tiempo, me beneficié del uso de métodos como Merise para establecer vínculos coherentes entre bases de datos, asegurando así una gestión eficiente de los datos e integración armoniosa con la aplicación. Mi comprensión avanzada del lenguaje SQL fue útil para crear consultas complejas y optimizadas en Microsoft SQL Server Management.`,
    text3Section2Cegi: `Finalmente, produje documentación técnica utilizando Swagger, una plataforma para crear especificaciones claras y precisas y documentación para APIs. Esta herramienta me permitió proporcionar información detallada sobre el funcionamiento de mi API, facilitando su uso y mantenimiento para desarrolladores posteriores.`,
    titleSection3Cegi: "Gestión de proyectos en Cegi Alfa",
    text1Section3Cegi: `Aunque encontré algunos desafíos iniciales, notablemente relacionados con la exploración en profundidad de tecnologías y modificaciones imprevistas, pude superar estos obstáculos y desarrollar una API funcional. Esta última está diseñada para registrar y manipular datos de acuerdo con las especificaciones del proyecto. Más específicamente, ofrece al usuario la capacidad de crear y actualizar un archivo de usuario, así como generar y modificar una solicitud de soporte.`,
    text2Section3Cegi: `Para el futuro, agregar un sistema de autenticación a la API parece una pista prometedora. Esta medida fortalecería la seguridad y la interoperabilidad del sistema, abriendo el camino a nuevas posibilidades y una evolución continua. Corresponde a la última tarea de mi planificación donde me encontré en autonomía.`,
    titleSection1Sylink: "Desarrollo en Sylink Technologie",
    text1Section1Sylink: `En el tercer año de mi Diploma Universitario de Tecnología en Producción Informática, durante mi año de alternancia en Sylink Technologie, trabajé en dos proyectos distintos destinados a fortalecer las capacidades de seguridad de las empresas. El primero consistió en desarrollar una API en ASP .NET Core para facilitar la recuperación de información sobre dispositivos dentro de la red de una organización a partir de bases de datos bajo PostgreSQL o QuestDB. Esta API permitía el procesamiento de datos en formato JSON para visualización, detección y análisis de amenazas potenciales.`,
    text2Section1Sylink: `Simultáneamente, lideré un proyecto de consola en C# que utilizaba Dehashed, un motor de búsqueda de bases de datos pirateadas. Este proyecto implicaba la creación de un sistema que permitía al usuario seleccionar una organización para generar informes sobre filtraciones de datos pirateados.`,
    text3Section1Sylink: `Luego implementé estos dos proyectos para probar con datos reales y compartir mi progreso con mis colegas utilizando el servidor WEB Microsoft IIS. Pero antes de eso, tuve que configurar una máquina en Proxmox. Estaba versionando mis proyectos para comunicarme más fácilmente sobre ellos.`,
    titleSection2Sylink: "Diseño y Documentación en Sylink Technologie",
    text1Section2Sylink: `Los diagramas UML y gráficos establecidos de antemano fueron invaluables para diseñar aplicaciones estructuradas y eficientes, promoviendo una mejor optimización del desarrollo.`,
    text2Section2Sylink: `Al mismo tiempo, utilicé métodos como Merise para establecer vínculos coherentes entre bases de datos, asegurando una gestión eficiente de los datos e integración armoniosa con la aplicación. Mi dominio avanzado del lenguaje SQL me permitió crear consultas complejas y optimizadas.`,
    text3Section2Sylink: `Finalmente, propuse integrar la documentación Swagger en mi proyecto de API, como aprendí durante mi pasantía anterior en Cegi Alfa. Esta documentación fue esencial para facilitar la comunicación con las partes interesadas y garantizar la sostenibilidad del proyecto, incluso en caso de adopción por otro desarrollador.`,
    titleSection3Sylink: "Colaboración en Sylink Technologie",
    text1Section3Sylink: `Trabajé de forma independiente en estos proyectos, mientras recibía comentarios regulares de mi supervisor alternante. También tuve que comunicarme con una empresa externa responsable de la visualización en 3D. Esto me permitió poner en práctica y mejorar mis habilidades de comunicación. También tuve la oportunidad de comunicarme con otros colegas especializados en tecnologías específicas.`,
    butName: "Diploma Universitario de Tecnología en Producción Informática",
    butInfo:
      "IUT de Clermont-Ferrand, 63 <br /> <strong>Especialidad:</strong> Desarrollo WEB",
    bacName: "Bachillerato",
    bacInfo:
      "Lycée George Sand, 36 <br /> <strong>Especialidades:</strong> N.S.I / Matemáticas / Física Química <br /> <strong>Mención:</strong> Bien",
    brevetName: "Diploma Nacional del Brevet des collèges",
    brevetInfo:
      "Collège Vincent Rotinat, 36 <br /> <strong>Mención:</strong> Muy bien",
    language: "Idiomas",
    french: "Francés",
    english: "Inglés",
    spanish: "Español",
    nativeLanguage: "Idioma nativo",
    intermediateAdvanced: "Intermedio avanzado (B2)",
    intermediate: "Intermedio (B1)",
    tools: "Herramientas",
    firstname: "Nombre",
    name: "Apellido",
    email: "Email",
    phone: "Teléfono",
    form: "Formulario",
    send: "Enviar",
    legal: "Aviso legal",
    edit: "Edición del sitio",
    descEdit: `Este sitio, accesible en la URL <a href="https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/">https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/</a>, es editado por:`,
    hugo: `Hugo PRADIER, residente en 11 Rue de l'Hôtel de Ville 63110 BEAUMONT, de nacionalidad francesa`,
    hosting: "Hospedaje",
    hostingDesc: `El sitio está alojado por la empresa GitHub, con sede en San Francisco.`,
    admin: "Administrador del sitio",
    adminDesc: `El administrador del sitio es Hugo PRADIER.`,
    contactMe: "Contáctame",
    phoneContact: "Por teléfono: +33768361359",
    emailContact: `Por correo electrónico: <a href="mailto:pradier.hugo36@gmail.com">pradier.hugo36@gmail.com</a>`,
    postalContact: `Por correo: 11 Rue de l'Hôtel de Ville 63110 BEAUMONT`,
    infoSection: "Informaciones",
    selectedLanguage: "Idioma",
  },
};

function setLanguage(lang) {
  document.querySelector('.nav-link[href="#accueil"]').innerText =
    translations[lang].accueil;
  document.querySelector('.nav-link[href="#apropos"]').innerText =
    translations[lang].apropos;
  document.querySelector('.nav-link[href="#experiences"]').innerText =
    translations[lang].experiences;
  document.querySelector('.nav-link[href="#formation"]').innerText =
    translations[lang].formation;
  document.querySelector('.nav-link[href="#competences"]').innerText =
    translations[lang].competences;
  document.querySelector('.nav-link[href="#contact"]').innerText =
    translations[lang].contact;
  document.querySelector("#accueil h1").innerHTML =
    translations[lang].accueilTitle;
  document.querySelector("#accueil h2").innerText =
    translations[lang].accueilSubtitle;
  document.querySelector("#telechargerCV").innerText =
    translations[lang].telechargerCV;
  document.querySelector("#contactezMoi").innerText =
    translations[lang].contactezMoi;
  document.querySelector("#titleAbout").innerText = translations[lang].apropos;
  document.querySelector("#bonjourTitle").innerText =
    translations[lang].bonjourTitle;
  document.querySelector("#bonjourContent1").innerText =
    translations[lang].bonjourContent1;
  document.querySelector("#bonjourContent2").innerText =
    translations[lang].bonjourContent2;
  document.querySelector("#bonjourContent3").innerText =
    translations[lang].bonjourContent3;
  document.querySelector("#bonjourContent4").innerText =
    translations[lang].bonjourContent4;
  document.querySelector("#bonjourContent5").innerText =
    translations[lang].bonjourContent5;
  document.querySelector("#bonjourContent6").innerText =
    translations[lang].bonjourContent6;
  document.querySelector("#experiences h2").innerText =
    translations[lang].experiencesTitle;
  document.querySelector("#sylinkTitle").innerText =
    translations[lang].sylinkTitle;
  document.querySelector("#sylinkDescription").innerHTML =
    translations[lang].sylinkDescription;
  document.querySelector("#cegiTitle").innerText = translations[lang].cegiTitle;
  document.querySelector("#cegiDescription").innerHTML =
    translations[lang].cegiDescription;
  document.querySelector("#developmentSylink").innerText =
    translations[lang].development;
  document.querySelector("#documentationSylink").innerText =
    translations[lang].documentation;
  document.querySelector("#collaborationSylink").innerText =
    translations[lang].collaboration;
  document.querySelector("#developmentCegi").innerText =
    translations[lang].development;
  document.querySelector("#documentationCegi").innerText =
    translations[lang].documentation;
  document.querySelector("#projectManagementCegi").innerText =
    translations[lang].projectManagement;
  document.querySelector("#titleSection1Cegi").innerText =
    translations[lang].titleSection1Cegi;
  document.querySelector("#text1Section1Cegi").innerText =
    translations[lang].text1Section1Cegi;
  document.querySelector("#text2Section1Cegi").innerText =
    translations[lang].text2Section1Cegi;
  document.querySelector("#titleSection2Cegi").innerText =
    translations[lang].titleSection2Cegi;
  document.querySelector("#text1Section2Cegi").innerText =
    translations[lang].text1Section2Cegi;
  document.querySelector("#text2Section2Cegi").innerText =
    translations[lang].text2Section2Cegi;
  document.querySelector("#text3Section2Cegi").innerText =
    translations[lang].text3Section2Cegi;
  document.querySelector("#titleSection3Cegi").innerText =
    translations[lang].titleSection3Cegi;
  document.querySelector("#text1Section3Cegi").innerText =
    translations[lang].text1Section3Cegi;
  document.querySelector("#text2Section3Cegi").innerText =
    translations[lang].text2Section3Cegi;
  document.querySelector("#titleSection1Sylink").innerText =
    translations[lang].titleSection1Sylink;
  document.querySelector("#text1Section1Sylink").innerText =
    translations[lang].text1Section1Sylink;
  document.querySelector("#text2Section1Sylink").innerText =
    translations[lang].text2Section1Sylink;
  document.querySelector("#text3Section1Sylink").innerText =
    translations[lang].text3Section1Sylink;
  document.querySelector("#titleSection2Sylink").innerText =
    translations[lang].titleSection2Sylink;
  document.querySelector("#text1Section2Sylink").innerText =
    translations[lang].text1Section2Sylink;
  document.querySelector("#text2Section2Sylink").innerText =
    translations[lang].text2Section2Sylink;
  document.querySelector("#text3Section2Sylink").innerText =
    translations[lang].text3Section2Sylink;
  document.querySelector("#titleSection3Sylink").innerText =
    translations[lang].titleSection3Sylink;
  document.querySelector("#text1Section3Sylink").innerText =
    translations[lang].text1Section3Sylink;
  document.querySelector("#tileFormation").innerText =
    translations[lang].formation;
  document.querySelector("#butName").innerText = translations[lang].butName;
  document.querySelector("#butInfo").innerHTML = translations[lang].butInfo;
  document.querySelector("#bacName").innerText = translations[lang].bacName;
  document.querySelector("#bacInfo").innerHTML = translations[lang].bacInfo;
  document.querySelector("#brevetName").innerText =
    translations[lang].brevetName;
  document.querySelector("#brevetInfo").innerHTML =
    translations[lang].brevetInfo;
  document.querySelector("#skill").innerText = translations[lang].competences;
  document.querySelector("#dev").innerText = translations[lang].development;
  document.querySelector("#lang").innerText = translations[lang].language;
  document.querySelector("#french").innerText = translations[lang].french;
  document.querySelector("#english").innerText = translations[lang].english;
  document.querySelector("#spanish").innerText = translations[lang].spanish;
  document.querySelector("#nativeLanguage").innerText =
    translations[lang].nativeLanguage;
  document.querySelector("#intermediateAdvanced").innerText =
    translations[lang].intermediateAdvanced;
  document.querySelector("#intermediate").innerText =
    translations[lang].intermediate;
  document.querySelector("#tools").innerText = translations[lang].tools;

  document.querySelector("#form").innerText = translations[lang].form;

  document.querySelector("#prenom").innerText = translations[lang].firstname;
  document.querySelector("#nom").innerText = translations[lang].name;
  document.querySelector("#mail").innerText = translations[lang].email;
  document.querySelector("#tel").innerText = translations[lang].phone;
  document.querySelector("#send").innerText = translations[lang].send;

  document.querySelector("#legal").innerText = translations[lang].legal;
  document.querySelector("#legalM").innerText = translations[lang].legal;
  document.querySelector("#edit").innerText = translations[lang].edit;
  document.querySelector("#descEdit").innerHTML = translations[lang].descEdit;
  document.querySelector("#hugo").innerHTML = translations[lang].hugo;
  document.querySelector("#hosting").innerText = translations[lang].hosting;
  document.querySelector("#hostingDesc").innerHTML =
    translations[lang].hostingDesc;
  document.querySelector("#admin").innerText = translations[lang].admin;
  document.querySelector("#adminDesc").innerHTML = translations[lang].adminDesc;
  document.querySelector("#contactMe").innerText = translations[lang].contact;
  document.querySelector("#phoneContact").innerText =
    translations[lang].phoneContact;
  document.querySelector("#emailContact").innerHTML =
    translations[lang].emailContact;
  document.querySelector("#postalContact").innerText =
    translations[lang].postalContact;
  document.querySelector("#contactSection").innerText =
    translations[lang].contact;
  document.querySelector("#infoSection").innerText =
    translations[lang].infoSection;
  document.querySelector("#selectedLanguage").innerText =
    translations[lang].selectedLanguage;
}
