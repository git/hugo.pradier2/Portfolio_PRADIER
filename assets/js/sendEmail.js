function sendEmail() {
  const firstName = document.getElementById("firstname").value;
  const name = document.getElementById("name").value;
  const email = document.getElementById("email").value;
  const phone = document.getElementById("phone").value;
  const message = document.getElementById("message").value;

  // Réinitialiser les messages d'erreur à chaque fois que la fonction est appelée
  document.getElementById("firstname-error").textContent = "";
  document.getElementById("name-error").textContent = "";
  document.getElementById("email-error").textContent = "";
  document.getElementById("phone-error").textContent = "";
  document.getElementById("message-error").textContent = "";

  // Vérifier que tous les champs sont remplis
  if (
    firstName.trim() === "" ||
    name.trim() === "" ||
    email.trim() === "" ||
    phone.trim() === "" ||
    message.trim() === ""
  ) {
    document.getElementById("response").textContent =
      "Veuillez remplir tous les champs.";
    return;
  }

  // Vérifier le format de l'e-mail avec une regex
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (!emailRegex.test(email)) {
    document.getElementById("email-error").textContent =
      "Veuillez entrer une adresse e-mail valide.";
    return;
  }

  // Vérifier le format du numéro de téléphone avec une regex
  const phoneRegex = /^\d{10}$/;
  if (!phoneRegex.test(phone)) {
    document.getElementById("phone-error").textContent =
      "Veuillez entrer un numéro de téléphone valide (10 chiffres).";
    return;
  }

  const serviceID = "service_v7bbwjc";
  const templateID = "template_rij5pjp";

  emailjs
    .send(serviceID, templateID, {
      firstname: firstName,
      name: name,
      email: email,
      phone: phone,
      message: message,
    })
    .then(
      () => {
        // alert("Votre message a bien été envoyé !");
        document.getElementById("response").textContent =
          "Votre message a bien été envoyé !";
        document.getElementById("contact-form").reset();
      },
      (err) => {
        // alert("Désolé, une erreur s'est produite. Veuillez réessayer.");
        document.getElementById("response").textContent =
          "Désolé, une erreur s'est produite. Veuillez réessayer.";
      }
    );
}
