[![Build Status](https://codefirst.iut.uca.fr/api/badges/hugo.pradier2/Portfolio_PRADIER/status.svg)](https://codefirst.iut.uca.fr/hugo.pradier2/Portfolio_PRADIER)

[Mon site](https://codefirst.iut.uca.fr/containers/hugopradier2-Portfolio_PRADIER/)

# Portfolio_PRADIER_Hugo_WEB2

J'ai réalisé ce portfolio en HTML et JavaScript en utilisant pour la première fois Bootstrap. Cela m'a permis de prendre en main cet outil, car il me fallait un projet concret pour en introduire les fondamentaux et l'utiliser efficacement. Je l'ai choisi après de nombreuses recommandations de mes collègues.

## Choix et justifications

### HTML et JavaScript

- **HTML** : Structure de base du site web.
- **JavaScript** : Ajout de fonctionnalités interactives, notamment pour le formulaire de contact et la gestion multi-langues.

### Bootstrap

- **Première utilisation** : J'ai choisi Bootstrap pour ce projet afin de me familiariser avec ce framework populaire. Cela m'a permis de créer un design simple, agréable et accessible.
- **Responsive Design** : Bootstrap facilite la création de sites web qui s'adaptent automatiquement aux différentes tailles d'écran, ce qui est essentiel pour garantir une bonne expérience utilisateur sur tous les appareils.

### EmailJS

- **Formulaire de contact** : Pour gérer l'envoi de messages depuis le formulaire de contact, j'ai intégré EmailJS. Cela m'a permis de configurer facilement l'envoi d'e-mails sans avoir besoin d'un backend complexe.

## Structure du portfolio

Voici les sections qui composent mon portfolio :

- Accueil : Introduction.
- À propos de moi : Informations personnelles et présentation.
- Expériences : Détail de mes expériences professionnelles.
- Formation : Parcours scolaire et diplômes.
- Compétences : Liste de mes compétences techniques et personnelles.
- Contact : Formulaire de contact pour me joindre facilement.
